#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os
import itertools
from hpccm.building_blocks import *
from hpccm.primitives import *


def generateRecipeBeso(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install needed packages
    Stage0 += packages(
        apt=[
            'libboost-all-dev',
            'lcov',
            'php',
            'php-xml',
            'xsltproc',
            'python'
            ],
        yum=[
            'boost-devel',
            'lcov',
            'php',
            'php-xml',
            'libxslt',
            'python2'
            ], epel=True)

    # pip install --upgrade pip
    # pip install gcovr
    Stage0 += pip(upgrade=True, packages=['gcovr'], pip='pip3')

    Stage0 += catalyst(edition='Base-Essentials-Extras', version='5.5.2',
                       prefix='/opt/catalyst')

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating App image definitions...")

    # list of combinations of configurations that can be used
    allConf = ["ubuntu18.04", "ubuntu20.04", "centos7"]
    allConf += combine(allConf, ["cuda10.1", "cuda10.2", "cuda11.2"])
    allConf = combine(allConf, ["mpich", "mpich-legacy",
                                "openmpi", "openmpi-legacy",
                                "mvapich2"])

    outputFolder = "Dockerfiles"

    for currConf in allConf:
        baseImageDev = "ompcluster/runtime:" + currConf
        baseImageExp = "ompcluster/runtime-dev:" + currConf

        # Generation of Dockerfile for BESO
        defFileTextBesoDev = str(generateRecipeBeso(baseImageDev))
        defFileTextBesoExp = str(generateRecipeBeso(baseImageExp))
        defFileNameBesoDev = "beso-dev-" + currConf
        defFileNameBesoExp = "beso-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameBesoDev}", "w") as text_file:
            text_file.write(str(defFileTextBesoDev))
        with open(f"{outputFolder}/{defFileNameBesoExp}", "w") as text_file:
            text_file.write(str(defFileTextBesoExp))


if __name__ == "__main__":
    main()
