# Building Runtime Images

This repository is used to build container images specifically for applications.
Those *applications* images contains the libraries to build application and
possibly the built applications.

The containers images are built automatically by CI/CD and are available on
the Docker Hub of the project (the name of the repository depends of the application):
https://hub.docker.com/r/ompcluster/

## Manual building

First, you need to install
[HPC Container Maker (HPCCM)](https://github.com/NVIDIA/hpc-container-maker),
an open-source tool to generates container specifications files (like
Dockerfiles). An HPCCM script is a Python recipe) that can generate multiple
container specifications.

HPCCM can be installed from  Pypi or Conda using the following commands:
```
sudo pip install hpccm
```
or
```
conda install -c conda-forge hpccm
```

Then, just run the following command to generate the definitions:
```
python3 hpcc-recipe.py
```

Finally, any of the generated images can be built using Docker or Singularity.
